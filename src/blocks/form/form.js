$('.form__accordion-btn').click(function(e) {
  e.preventDefault();
  $(this).hide();
  $(this).closest('.form__accordion').find('.form__accordion-content').fadeIn('fast');
})
